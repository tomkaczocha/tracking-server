const express = require('express');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

const app = express();

app.disable('x-powered-by');
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(bodyParser.json());
app.use((req, res, next) => {
  console.log(`Handling ${req.path}`);

  next();
});

app.get('/', async (req, res) => {
  return res.status(200).json({
    status: 'OK',
  });
});

app.post('/shipment', (req, res) => {
  console.log('Tracking shipment ', req.body);

  return res.status(200).json({
    tracking: true,
  });
});

app.get('/health', async (req, res) => {
  return res.status(200).send('OK');
});

// error handler
app.use((error, req, res, next) => {
  console.error(error);

  return res.status(500).json({
    error: 'Server failed processing the request',
  });
});

app.listen(port);

console.log(`Server running at http://localhost:${port}`);
